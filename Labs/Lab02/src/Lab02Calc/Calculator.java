package Lab02Calc;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * Servlet implementation class Calculator
 */
@WebServlet(description = "Servlet that calculates", urlPatterns = { "/Calculator" })
public class Calculator extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Calculator() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String valadd1, valadd2, valmult1, valmult2, valsum, valproduct;
 		String add1 = request.getParameter("add1").trim() ;
		String add2 = request.getParameter("add2").trim();
		String mult1 = request.getParameter("mult1").trim();
		String mult2 = request.getParameter("mult2").trim();

		String sum = "";
		String product = "";
		
		// Check for valid inputs....
		try {
			int a1 = Integer.parseInt(add1);
			int a2 = Integer.parseInt(add2);
			sum = "" + (a1+a2);
		} catch(NumberFormatException e) {
			if (add1.isEmpty()) add1 = "''";
			if (add2.isEmpty()) add2 = "''";
			sum = "''";
		}
		
		/*
		 * Integer.parseInt(str) throws NumberFormatException 
		 * if the string cannot be converted to an integer.
		 */
		
		try {
			int m1 = Integer.parseInt(mult1);
			int m2 = Integer.parseInt(mult2);
			product = "" + (m1 * m2);
		} catch(NumberFormatException e) {
			if (mult1.isEmpty()) mult1 = "''";
			if (mult2.isEmpty()) mult2 = "''";
			product = "''";
		}
		request.setAttribute("add1", add1);
		request.setAttribute("add2", add2);
		request.setAttribute("mult1", mult1);
		request.setAttribute("mult2", mult2);
		request.setAttribute("result1", sum);
		request.setAttribute("result2", product);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/JSP/result.jsp");
		rd.forward(request,response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
