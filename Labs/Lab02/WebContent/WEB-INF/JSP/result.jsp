<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>The result</title>
</head>
<body>
<%=request.getAttribute("add1") %>+ <%=request.getAttribute("add2") %> = <%=request.getAttribute("result1") %>
 <%=request.getAttribute("mult1") %>* <%=request.getAttribute("mult2") %> = <%=request.getAttribute("result2") %>
</body>
</html>