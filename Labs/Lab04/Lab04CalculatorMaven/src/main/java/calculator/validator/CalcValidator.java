package calculator.validator;

import java.util.List;

public interface CalcValidator {
	
	public List<String> validate(Object  object ) ;
}
