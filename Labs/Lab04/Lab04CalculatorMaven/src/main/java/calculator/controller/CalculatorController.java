package calculator.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import calculator.domain.Calculation;
import calculator.validator.CalcValidator;
import calculator.serviceImpl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CalculatorController {
	
	@Autowired
	CalcValidator calcValidator;
	
	@Autowired
	CalculateServiceImpl calcService;

	
	@RequestMapping(value={"/"}, method = RequestMethod.GET)
	public String showCalc()
	{
		
		return "calculatorForm";
	}
	
	@RequestMapping(value={"/calculator"}, method = RequestMethod.GET)
	public String getresult(Calculation calculation, Model model)
	{
		String result="";
		List<String> errors = calcValidator.validate(calculation);
		int x = 0;
		
		if (!errors.isEmpty())
		{
			for (String err : errors)
			{
				x++;
				result += String.format("Error %d: %s ;", x,err);
			}
			
		}
		else
		{
			//calculation
			result = calcService.Calculate(calculation);
		}
	
		
		model.addAttribute("result", result);
		return "result";
		
	}


}
