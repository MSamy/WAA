package calculator.service;

import calculator.domain.Calculation;

public interface CalculateService {
	
	public String Calculate(Calculation c);

}
