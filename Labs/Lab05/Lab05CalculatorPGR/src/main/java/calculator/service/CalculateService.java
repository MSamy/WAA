package calculator.service;

import calculator.domain.Calculation;

public interface CalculateService {
	
	public String Calculate(Calculation c);
	public Calculation CalculateObject(Calculation c);

}
