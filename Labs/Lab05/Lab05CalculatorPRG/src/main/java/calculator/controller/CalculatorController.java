package calculator.controller;

import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import calculator.domain.Calculation;
import calculator.validator.CalcValidator;

import calculator.serviceImpl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class CalculatorController {
	
	@Autowired
	CalcValidator calcValidator;
	
	@Autowired
	CalculateServiceImpl calcService;

	
	@RequestMapping(value={"/"}, method = RequestMethod.GET)
	public String showCalc()
	{
		
		return "calculatorForm";
	}
	
	@RequestMapping(value={"/calculator"}, method = RequestMethod.POST)
	public String getresult(@RequestParam Calculation calculation, RedirectAttributes redirectAttributes)
	{
		
		String result="";
		List<String> errors = calcValidator.validate(calculation);
		int x = 0;
		
		if (!errors.isEmpty())
		{
			for (String err : errors)
			{
				x++;
				result += String.format("Error %d: %s ;", x,err);
			}
			calculation.setErrors(result);
			
		}
		else
		{
			//calculation
			calculation = calcService.CalculateObject(calculation);
			redirectAttributes.addFlashAttribute(calculation);
		}
	
		System.out.println(calculation.toString());
		//String ret = String.format("redirect:calculatorView.jsp?add1=%f&add2=%f&amult1=%f&mult2=%f&sum=%f&product=%f&errors=%s", calculation.getAdd1(), calculation.getAdd2(), calculation.getSum(), calculation.getMult1(), calculation.getMult2(), calculation.getProduct(), calculation.getErrors());
		//model.addAttribute("result", result);
		return "calculatorForm";
		//return "redirect:results";
	}

	
	 @RequestMapping(value="/results", method = RequestMethod.GET)
	  public String calculatorResults(Model model) throws IOException {

	  	Calculation calculation = (Calculation)( ((ModelMap) model).get("calculation") );

	  	if (calculation == null)
	  		throw new IOException("The calculation is Obsolete, Try Again!");
	  	  
	        return "CalculatorView";
	  }
}
