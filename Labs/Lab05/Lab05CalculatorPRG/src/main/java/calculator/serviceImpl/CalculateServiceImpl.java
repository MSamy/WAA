package calculator.serviceImpl;

import org.springframework.stereotype.Service;

import calculator.domain.Calculation;
import calculator.service.CalculateService;

@Service
public class CalculateServiceImpl implements CalculateService {

	public String Calculate(Calculation c) {
		
		//calculate
		
		String result = "";
		double add1 = c.getAdd1();
		double add2 = c.getAdd2();
		double mult1 = c.getMult1();
		double mult2 = c.getMult2();
		double sum = add1+ add2;
		double prod = mult1 * mult2;
		
		
		result += String.format("%f + %f = %f /n %f * %f = %f",add1, add2,sum, mult1, mult2,prod);
		return result;
		
		
	}

	public Calculation CalculateObject(Calculation c) {
		
		double add1	= c.getAdd1();
		double add2	= c.getAdd2();
		c.setSum(add1+add2);
		double mult1 = c.getMult1();
		double mult2 = c.getMult2();
		c.setProduct(mult1*mult2);
		
		
		
	
		return c;
		
	}

}
