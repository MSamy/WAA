package calc.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import calc.domain.Calculation;

import mum.edu.framework.annotation.AutoWired;
import mum.edu.framework.annotation.RequestMapping;
import mum.edu.framework.controller.Controller;
import calc.validator.CalcValidator;

public class CalcController implements Controller 
{
	@AutoWired
	CalcValidator calcValidator;

	@RequestMapping(value={"/","/getresult"})
	public String getresult(Calculation calculation, HttpServletRequest request, 
			HttpServletResponse response)
	{
		String result="";
		List<String> errors = calcValidator.validate(calculation);
		int x = 0;
		
		if (!errors.isEmpty())
		{
			for (String err : errors)
			{
				x++;
				result += String.format("Error %s: %s2 ;", x,err);
			}
			return result;
		}
	
		//calculation
		result = "";
		
		return null;
		
	}
}

