package calc.domain;

public class Calculation {
	
	private double add1;
	private double add2;
	private double mult1;
	private double mult2;
	
	public double getAdd1() {
		return add1;
	}
	public void setAdd1(double add1) {
		this.add1 = add1;
	}
	public double getMult2() {
		return mult2;
	}
	public void setMult2(double mult2) {
		this.mult2 = mult2;
	}
	public double getAdd2() {
		return add2;
	}
	public void setAdd2(double add2) {
		this.add2 = add2;
	}
	public double getMult1() {
		return mult1;
	}
	public void setMult1(double mult1) {
		this.mult1 = mult1;
	}

}
