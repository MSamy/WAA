package calc.validator;

import java.util.ArrayList;
import java.util.List;

import calc.domain.Calculation;

public class CalcValidatorImpl implements CalcValidator {

	@Override
	public List<String> validate(Object object) {
		
		Calculation calc = (Calculation)object;
		List<String> errors = new ArrayList<String>();
		
		//validation calc attributes
		Double add1 = calc.getAdd1();
		if (add1 == null)
		{
			errors.add("add1 is empyty");
		}
		
		Double add2 = calc.getAdd2();
		if (add2 == null)
		{
			errors.add("add2 is empyty");
		}
		
		
		return errors;
	}

}
