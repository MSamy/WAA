package app07.repository;

import java.util.List;

import app07.domain.Category;
import app07.domain.Product;

 public interface CategoryRepository   {
	
 
		public Category getCategory(int id);
		
		public List<Category> getAll();
		   
	
}
 
