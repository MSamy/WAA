package app07.repository;

import java.util.List;

import app07.domain.Category;
import app07.domain.Product;

 public interface ProductRepository   {
	
 
	
	public List<Product> getAll();
	
	public void save(Product product);
	
}
 
