package app07.service;

import java.util.List;

import app07.domain.Product;

 public interface ProductService   {
	
	public List<Product> getAll();
	
	public void save(Product product);
	
	
		   
}
 
