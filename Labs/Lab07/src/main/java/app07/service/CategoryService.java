package app07.service;

import java.util.List;

import app07.domain.Category;
import app07.domain.Product;

 public interface CategoryService  {	
		public Category getCategory(int id);	
		public List<Category> getAll();	
}
 
