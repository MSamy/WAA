package app07.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app07.domain.Category;
import app07.domain.Product;
import app07.repository.CategoryRepository;
import app07.repository.ProductRepository;
import app07.service.CategoryService;
import app07.service.ProductService;

@Service
public class CategoryServiceImpl implements CategoryService {
	
	@Autowired
	CategoryRepository categoryRepository;
	
	@Override
  	public List<Category> getAll() {
		return categoryRepository.getAll();
	}

	@Override
	public Category getCategory(int id) {
		return categoryRepository.getCategory(id);
 	}
	
	
		   
}
 
